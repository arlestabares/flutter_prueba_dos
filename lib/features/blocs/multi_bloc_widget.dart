import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart'
    as home_bloc;
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<home_bloc.Bloc>(
          create: (context) =>
              home_bloc.Bloc()..add(ProductListEvent()),
        )
      ],
      child: child,
    );
  }
}
