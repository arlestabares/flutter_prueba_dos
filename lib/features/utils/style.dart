import 'package:flutter/material.dart';
import 'package:prueba_dos/features/utils/const.dart';

const  style = TextStyle(fontSize: 21.0);

final ButtonStyle outLinedButtonStyle = OutlinedButton.styleFrom(
  primary: Colors.white,
  backgroundColor: colorPrimary,
  minimumSize:const Size(0, 46),
  padding:const EdgeInsets.symmetric(horizontal: 76.0),
  shape:const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(24)),
  ),
);