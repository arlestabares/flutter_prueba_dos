import 'package:flutter/material.dart';

const colorPrimary= Color(0xFF0D873C);
const kTextColor = Color(0xFF676769);
const colorText= Color(0xFF2D2D2D);
const colorButtonClosed= Color(0xFFA0A0A9);

const kDefaultPadding= 20.0;