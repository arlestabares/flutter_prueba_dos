class Product {
  bool? isSelected;
  int? id;
  String? image;
  String? title;
  String? gr;
  String? subtitle;
  double? price;
  int? quantity;
  bool? offer;

  Product({
    this.id,
    this.isSelected,
    this.image,
    this.title,
    this.gr,
    this.subtitle,
    this.price,
    this.quantity,
    this.offer = false,
  });
  Product.fromJson(Map<dynamic, dynamic> json)
      : id = json['id'],
        image = json['image'],
        title = json['title'],
        gr = json['gr'],
        subtitle = json['subtitle'],
        price = json['price'],
        quantity = json['amount'],
        offer = json['offer'];
}

List<Product> productList = [
  Product(
    id: 1,
    isSelected: false,
    image: 'assets/images/img_1.png',
    title: 'Almendra Cubierta en Chocolate - Gozana',
    gr: '12 gr',
    subtitle: 'Gozana',
    price: 15.00,
    quantity: 0,
    offer: false,
  ),
  Product(
    id: 2,
    isSelected: false,
    image: 'assets/images/img_2.png',
    title: 'Garbanzos Horneados Ajo y Cebolla - Gozana',
    gr: '90 gr',
    subtitle: 'Gozana Snacks',
    price: 11.00,
    quantity: 0,
    offer: false,
  ),
  Product(
    id: 3,
    isSelected: false,
    image: 'assets/images/img_3.png',
    title: 'Cacahuetes Horneados con Maiz - Gozana',
    gr: '60 gr',
    subtitle: 'Gozana Snacks',
    price: 18.00,
    quantity: 0,
    offer: false,
  ),
  Product(
    id: 4,
    isSelected: false,
    image: 'assets/images/img_4.png',
    title: 'Nueces de Huesca con mermelada la  - Gozana',
    gr: '80 gr',
    subtitle: 'Gozana Snacks',
    price: 21.00,
    quantity: 0,
    offer: false,
  ),
  Product(
    id: 5,
    isSelected: false,
    image: 'assets/images/img_4.png',
    title: 'Nueces de Huesca la - Gozana',
    gr: '80 gr',
    subtitle: 'Gozana Snacks',
    price: 27.00,
    quantity: 0,
    offer: false,
  ),
  Product(
    id: 6,
    isSelected: false,
    image: 'assets/images/img_4.png',
    title: 'Nueces de Huesca la - Gozana',
    gr: '80 gr',
    subtitle: 'Gozana Snacks',
    price: 14.00,
    quantity: 0,
    offer: false,
  ),
];
