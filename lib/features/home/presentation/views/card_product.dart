import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/data/models/product.dart';
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart' as bloc;
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart';
import 'package:prueba_dos/features/utils/const.dart';
import '../show_modal/widgets/widgets_modal.dart';
import '../widgets/widgets.dart';

class CardProduct extends StatelessWidget {
  const CardProduct({Key? key, required this.product}) : super(key: key);
  final Product product;
  @override
  Widget build(BuildContext context) {
    // int count = 0;
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                height: 120.0,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(13.0),
                    topRight: Radius.circular(13.0),
                  ),
                  image: DecorationImage(
                    image: AssetImage('${product.image}'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              product.offer!
                  ? Container(
                      height: 70.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(51.0),
                      ),
                      child: const Center(
                        child: Text('En oferta}'),
                      ),
                    )
                  : const SizedBox.shrink()
            ],
          ),
          const SizedBox(height: 8.0),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextWidget(
                  text: product.title!,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
                TextWidget(
                    text: product.gr!, style: const TextStyle(fontSize: 16.0)),
                TextWidget(
                    text: product.subtitle!,
                    style:
                        const TextStyle(fontSize: 18.0, color: colorPrimary)),
                TextWidget(
                  text: '/\$ ${product.price!}',
                  style: const TextStyle(fontSize: 21.0),
                ),
              ],
            ),
          ),
          const SizedBox(height: 12.0),
          BlocBuilder<bloc.Bloc, bloc.State>(
            buildWhen: (_, state) =>
                state is ProductListState ||
                state is AddProductState ||
                state is RemoveProductState,
            builder: (BuildContext context, state) {
              if (state is ProductListState ||
                  state is AddProductState ||
                  state is RemoveProductState) {
                return !product.isSelected!
                    ? Center(
                        child: ElevatedButtonWidget(
                          sizeWith: 0.4,
                          text: 'Agregar',
                          onPressed: () {
                            // product.quantity= 1;
                            context
                                .read<bloc.Bloc>()
                                .add(AddProductEvent(product));
                          },
                        ),
                      )
                    : Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16.0),
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        height: 50.0,
                        decoration: BoxDecoration(
                          color: colorPrimary,
                          borderRadius: BorderRadius.circular(51.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: 40.0,
                              width: 40.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(51.0),
                              ),
                              child: Center(
                                child: IconButton(
                                  onPressed: () {
                                    if (product.quantity! > 0) {
                                      context
                                          .read<bloc.Bloc>()
                                          .add(RemoveProductEvent(product));
                                    }
                                  },
                                  icon: const Icon(Icons.remove),
                                ),
                              ),
                            ),
                            product.quantity! > 0
                                ? BlocBuilder<bloc.Bloc, bloc.State>(
                                    builder: (context, state) {
                                      if (state is ProductListState ||
                                          state is AddProductState ||
                                          state is RemoveProductState) {
                                        return TextWidget(
                                          text: '${product.quantity}',
                                        );
                                      }
                                      return const SizedBox.shrink();
                                    },
                                  )
                                : const SizedBox.shrink(),
                            Container(
                              height: 40.0,
                              width: 40.0,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(51.0),
                              ),
                              child: Center(
                                child: IconButton(
                                  onPressed: () {
                                    context
                                        .read<bloc.Bloc>()
                                        .add(AddProductEvent(product));
                                  },
                                  icon: const Icon(Icons.add),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
              }
              return const Center(child: CircularProgressIndicator());
              // return
            },
          )
        ],
      ),
    );
  }
}
