import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:prueba_dos/features/home/presentation/views/product_view.dart';
// import 'package:prueba_dos/features/home/presentation/views/product_view.dart';
import 'package:prueba_dos/features/utils/const.dart';

import '../show_modal/modal.dart';
import '../widgets/widgets.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: appBarInicio(),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 0.0),
        child: Column(
          children: [
            Container(height: 8.0, color: Colors.grey.withOpacity(0.2)),
            Container(
              color: Colors.grey.withOpacity(0.2),
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.arrow_back,
                          color: colorPrimary,
                          size: 41.0,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          height: 50.0,
                          child: TextField(
                            decoration: InputDecoration(
                              hintText: 'Buscar en market',
                              border: OutlineInputBorder(
                                // borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(21.0),
                              ),
                              suffixIcon: Container(
                                decoration: const BoxDecoration(
                                  color: colorPrimary,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(12.0),
                                    bottomRight: Radius.circular(12.0),
                                  ),
                                ),
                                width: 71.0,
                                child: IconButton(
                                    onPressed: () {},
                                    icon: const Icon(
                                      Icons.search,
                                      color: Colors.white,
                                      size: 31,
                                    )),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Row(
                      children: [
                        const Icon(Icons.pin_drop_sharp),
                        const SizedBox(width: 21.0),
                        const Expanded(
                          child: Text(
                            'Consultar cobertura',
                            style: TextStyle(
                                fontSize: 21.0, fontWeight: FontWeight.w400),
                          ),
                        ),
                        IconButton(
                          onPressed: () async {
                            showModalCobertura(context, size);
                          },
                          icon: const Icon(
                            Icons.arrow_drop_down_sharp,
                            size: 41.0,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            // const SizedBox(height: 8.0),
            Row(
              children: const [
                Text(
                  'Snacks',
                  style: TextStyle(fontSize: 31.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xFFF4F4F4),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  width: size.width * 0.4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Ordenar',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      IconButton(
                        onPressed: () {
                          showModalOrdenar(context, size);
                        },
                        icon: const FaIcon(FontAwesomeIcons.arrowsUpDown),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xFFF4F4F4),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  width: size.width * 0.4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Marcas',
                        style: TextStyle(fontSize: 16.0),
                      ),
                      IconButton(
                        onPressed: () async {
                          showModalMarcas(context, size);
                        },
                        icon: const Icon(
                          Icons.arrow_drop_down_sharp,
                          size: 41.0,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            const EtiquetasBuilder(),
            const ProductView(),
           
          ],
        ),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(),
    );
  }
}
