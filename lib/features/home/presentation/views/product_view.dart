import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart' as bloc;
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart';
import 'package:prueba_dos/features/home/presentation/views/card_product.dart';

class ProductView extends StatelessWidget {
  const ProductView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.width > 400 ? size.height * 0.5 : size.height * 0.4,
      child: BlocBuilder<bloc.Bloc, bloc.State>(
        buildWhen: (_, state) => state is ProductListState,
        builder: (BuildContext context, state) {
          if (state is ProductListState ||
              state is AddProductState ||
              state is RemoveProductState) {
            return GridView.builder(
              shrinkWrap: true,
              itemCount: state.model.productList?.length,
              gridDelegate:  SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio:  size.width > 400?  0.6 : 0.5
                // childAspectRatio: MediaQuery.of(context).size.width /
                //     (MediaQuery.of(context).size.height / 0.9),
              ),
              itemBuilder: (context, int index) {
                final product = state.model.productList?[index];
                return CardProduct(
                  product: product!,
                );
              },
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
