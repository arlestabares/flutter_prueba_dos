import 'package:bloc/bloc.dart' as bloc;
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/data/models/product.dart';

part 'event.dart';
part 'state.dart';

class Bloc extends bloc.Bloc<Event, State> {
  Bloc() : super(initialState) {
    on<ShangeStatusEvent>(_onShangeStatus);
    on<MostRelevantProductEvent>(_onMostRelevantProductEvent);
    on<LowerPricedProductEvent>(_onLowerPricedProductEvent);
    on<HigherPricedProductEvent>(_onHigherPricedProductEvent);
    on<OffersHighestToLowestEvent>(_onOffersHighestToLowestEvent);
    on<LakantoProductEvent>(_onLakantoProductEvent);
    on<NutriShakeProductEvent>(_onNutriShakeProductEvent);
    on<MarcaComercialProductEvent>(_onMarcaComercialProductEvent);
    on<SnackSantoProductEvent>(_onSnackSantoProductEvent);
    on<ProductListEvent>(_onProductListStateEvent);
    on<ProductSelectedListEvent>(_onProductSelectedListEvent);
    on<AddProductEvent>(_onAddProductEvent);
    on<RemoveProductEvent>(_onRemoveProductEvent);
  }
  static State get initialState => const InitialState(Model());
  _onShangeStatus(ShangeStatusEvent event, Emitter<State> emit) {
    emit(ShangeStatusState(
        state.model.copiWith(isMoreRelevant: event.isMoreRelevant)));
  }

  _onMostRelevantProductEvent(
      MostRelevantProductEvent event, Emitter<State> emit) {
    emit(MostRelevantProductState(state.model));
  }

  _onLowerPricedProductEvent(
      LowerPricedProductEvent event, Emitter<State> emit) {
    emit(LowerPricedProductState(state.model));
  }

  _onHigherPricedProductEvent(
      HigherPricedProductEvent event, Emitter<State> emit) {
    emit(LowerPricedProductState(state.model));
  }

  _onOffersHighestToLowestEvent(
      OffersHighestToLowestEvent event, Emitter<State> emit) {
    emit(HigherPricedProductState(state.model));
  }

  _onLakantoProductEvent(LakantoProductEvent event, Emitter<State> emit) {
    emit(LakantoProductState(state.model));
  }

  _onNutriShakeProductEvent(NutriShakeProductEvent event, Emitter<State> emit) {
    emit(NutriShakeProductState(state.model));
  }

  _onMarcaComercialProductEvent(
      MarcaComercialProductEvent event, Emitter<State> emit) {
    emit(MarcaComercialProductState(state.model));
  }

  _onSnackSantoProductEvent(SnackSantoProductEvent event, Emitter<State> emit) {
    emit(SnackSantoProductState(state.model));
  }

  _onProductListStateEvent(ProductListEvent event, Emitter<State> emit) {
    final productsList = productList;
    emit(ProductListState(state.model.copiWith(productList: productsList)));
  }

  _onProductSelectedListEvent(
      ProductSelectedListEvent event, Emitter<State> emit) {
    emit(ProductSeletedListState(state.model));
  }

  _onAddProductEvent(AddProductEvent event, Emitter<State> emit) {
    final productList = state.model.productList;
    var productInList =
        productList!.firstWhere((producto) => producto.id == event.product.id);
    productInList.quantity = (productInList.quantity ?? 0) + 1;
    productInList.isSelected = true;
    emit(
      AddProductState(state.model),
    );
  }

  _onRemoveProductEvent(RemoveProductEvent event, Emitter<State> emit) {
    var productInList =
        productList.firstWhere((producto) => producto.id == event.product.id);

    productInList.quantity = productInList.quantity! - 1;
    if (productInList.quantity == 0) {
      productInList.isSelected = false;
    }

    emit(RemoveProductState(state.model));
  }
}
