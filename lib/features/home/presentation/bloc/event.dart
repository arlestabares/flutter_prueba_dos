part of 'bloc.dart';

abstract class Event extends Equatable {
  const Event();

  @override
  List<Object> get props => [];
}

class ShangeStatusEvent extends Event {
  final bool? isMoreRelevant;

  const ShangeStatusEvent({this.isMoreRelevant});
}

class MostRelevantProductEvent extends Event {}

class LowerPricedProductEvent extends Event {}

class HigherPricedProductEvent extends Event {}

class OffersHighestToLowestEvent extends Event {}

class LakantoProductEvent extends Event {}

class NutriShakeProductEvent extends Event {}

class MarcaComercialProductEvent extends Event {}

class SnackSantoProductEvent extends Event {}

class ProductListEvent extends Event {}

class ProductSelectedListEvent extends Event {}

class AddProductEvent extends Event {
  final Product product;

  const AddProductEvent(this.product);
}

class RemoveProductEvent extends Event {
  final Product product;

  const RemoveProductEvent(this.product);
}
