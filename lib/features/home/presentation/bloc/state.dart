part of 'bloc.dart';

abstract class State extends Equatable {
  const State(this.model);
  final Model model;

  @override
  List<Object> get props => [];
}

class InitialState extends State {
  const InitialState(Model model) : super(model);
}

class ShangeStatusState extends State {
  const ShangeStatusState(Model model) : super(model);
}

class MostRelevantProductState extends State {
  const MostRelevantProductState(Model model) : super(model);
}

class LowerPricedProductState extends State {
  const LowerPricedProductState(Model model) : super(model);
}

class HigherPricedProductState extends State {
  const HigherPricedProductState(Model model) : super(model);
}

class LakantoProductState extends State {
  const LakantoProductState(super.model);
}

class NutriShakeProductState extends State {
  const NutriShakeProductState(super.model);
}

class MarcaComercialProductState extends State {
  const MarcaComercialProductState(super.model);
}

class SnackSantoProductState extends State {
  const SnackSantoProductState(super.model);
}

class ProductListState extends State {
  const ProductListState(super.model);
}

class ProductSeletedListState extends State {
  const ProductSeletedListState(super.model);
}

class AddProductState extends State {
  const AddProductState(super.model);
}

class RemoveProductState extends State {
  const RemoveProductState(super.model);
}

class Model extends Equatable {
  final bool? isMoreRelevant;
  final List<Product>? productList;
  final List<Product>? addProductList;
  final int? quantity;
  final bool? isSelected;
  final Product? product;

  const Model({
    this.quantity,
    this.product,
    this.isSelected,
    this.isMoreRelevant,
    this.productList,
    this.addProductList,
  });

  Model copiWith({
    int? quantity,
    bool? isSelected,
    Product? product,
    bool? isMoreRelevant,
    List<Product>? productList,
    List<Product>? addProductList,
  }) {
    return Model(
      quantity: quantity ?? this.quantity,
      product: product ?? this.product,
      isSelected: isSelected ?? this.isSelected,
      isMoreRelevant: isMoreRelevant ?? this.isMoreRelevant,
      productList: productList ?? this.productList,
      addProductList: addProductList ?? this.addProductList,
    );
  }

  @override
  List<Object?> get props => [
        isMoreRelevant,
        productList,
        addProductList,
        quantity,
        isSelected,
        product
      ];
}
