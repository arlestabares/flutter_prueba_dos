import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart'
    as home_bloc;
import 'package:prueba_dos/features/home/presentation/show_modal/widgets/global_modal_container.dart';

import '../../../utils/const.dart';
import '../widgets/widgets.dart';

Future<void> showModalOrdenar(BuildContext context, Size size) {
  bool isValueRelevante = false;
  bool isValueMenorPrecio = false;
  bool isValueMayorPrecio = false;
  bool isValueOfertas = false;
  return showModalBottomSheet<void>(
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(21.0),
        topRight: Radius.circular(21.0),
      ),
    ),
    context: context,
    builder: (BuildContext context) {

      return StatefulBuilder(
        builder:
            (BuildContext context, void Function(void Function()) setState) {
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 1.0),
            height: ( size.width > 420.0) ? size.height * 0.37: size.height * 0.5,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Container(
                          height: 25.0,
                          width: 25.0,
                          decoration: BoxDecoration(
                            color: colorButtonClosed,
                            borderRadius: BorderRadius.circular(21.0),
                          ),
                          child: const Icon(
                            Icons.close,
                            color: Colors.white,
                          )),
                    )
                  ],
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const <Widget>[
                          TextWidget(
                            text: 'Ordenar por',
                          ),
                        ],
                      ),
                      const SizedBox(height: 31.0),
                      GlobalModalContainer(
                          child: CheckBoxContainer(
                        isCheckbox: true,
                        text: 'Mas relevante',
                        isValue: isValueRelevante,
                        onChanged: (value) {
                          setState(() {
                            isValueRelevante = value!;
                          });
                          context
                              .read<home_bloc.Bloc>()
                              .add(home_bloc.MostRelevantProductEvent());
                        },
                      )),
                      GlobalModalContainer(
                          child: CheckBoxContainer(
                        isCheckbox: true,
                        text: 'Menor precio',
                        isValue: isValueMenorPrecio,
                        onChanged: (value) {
                          setState(() {
                            isValueMenorPrecio = value!;
                          });
                          context
                              .read<home_bloc.Bloc>()
                              .add(home_bloc.LowerPricedProductEvent());
                        },
                      )),
                      GlobalModalContainer(
                          child: CheckBoxContainer(
                        isCheckbox: true,
                        text: 'Mayor precio',
                        isValue: isValueMayorPrecio,
                        onChanged: (value) {
                          setState(() {
                            isValueMayorPrecio = value!;
                          });
                          context
                              .read<home_bloc.Bloc>()
                              .add(home_bloc.HigherPricedProductEvent());
                        },
                      )),
                      GlobalModalContainer(
                          child: CheckBoxContainer(
                        isCheckbox: true,
                        text: 'Ofertas (Mayor a menor precio)',
                        isValue: isValueOfertas,
                        onChanged: (value) {
                          setState(() {
                            isValueOfertas = value!;
                          });
                          context
                              .read<home_bloc.Bloc>()
                              .add(home_bloc.OffersHighestToLowestEvent());
                        },
                      )),
                    ],
                  ),
                )
              ],
            ),
          );
        },
      );
    },
  );
}
