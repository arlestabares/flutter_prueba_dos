import 'package:flutter/material.dart';

import '../../widgets/widgets.dart';

class TitleAndSubtitleContainer extends StatelessWidget {
  const TitleAndSubtitleContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const <Widget>[
              TextWidget(
                text: 'Consulta tu cobertura',
              ),
            ],
          ),
          const SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              TextWidget(
                style: Theme.of(context)
                    .textTheme
                    .titleMedium!
                    .copyWith(fontSize: 12.0),
                text: '¿A donde te llevamos tus pedidos?',
              ),
            ],
          ),
        ],
      ),
    );
  }
}