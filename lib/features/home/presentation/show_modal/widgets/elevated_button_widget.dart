import 'package:flutter/material.dart';

import '../../../../utils/const.dart';

class ElevatedButtonWidget extends StatelessWidget {
  const ElevatedButtonWidget({
    Key? key,
    required this.text,
    this.buttonStyle,
    this.textStyle,
    required this.onPressed,
    this.sizeWith =0.9,
  }) : super(key: key);
  final String text;
  final ButtonStyle? buttonStyle;
  final TextStyle? textStyle;
  final void Function()? onPressed;
   final num? sizeWith;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final ButtonStyle outLinedButtonStyle = OutlinedButton.styleFrom(
      elevation: 10.0,
      fixedSize: Size(size.width * 0.9, 46),
      primary: Colors.white,
      backgroundColor: colorPrimary,
      minimumSize: const Size(0, 46),
      maximumSize: Size(size.width *  sizeWith!, 46),
      // padding: EdgeInsets.symmetric(horizontal: size.width *  0.1),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(24)),
      ),
    );
    return ElevatedButton(
      style: buttonStyle ?? outLinedButtonStyle,
      onPressed: onPressed,
      child: Text(
        text,
        maxLines: 1,
        style: textStyle ??
            Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(fontSize: 21.0, color: Colors.white),
      ),
    );
  }
}
