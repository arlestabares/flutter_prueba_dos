import 'package:flutter/material.dart';

import '../../../../utils/const.dart';

class TextButtonWidget extends StatelessWidget {
  const TextButtonWidget({
    Key? key,
    required this.onPressed,
  }) : super(key: key);
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        'Ubicacion actual',
        style: Theme.of(context)
            .textTheme
            .titleSmall!
            .copyWith(color: colorPrimary),
      ),
    );
  }
}
