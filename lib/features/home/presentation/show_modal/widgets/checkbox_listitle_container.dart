import 'package:flutter/material.dart';

import '../../../../utils/const.dart';

class CheckBoxListTileContainer extends StatelessWidget {
  const CheckBoxListTileContainer(
      {Key? key,
      required this.text,
      required this.onChanged,
      required this.isValue})
      : super(key: key);
  final String text;
  final Function(bool? value) onChanged;
  final bool isValue;
  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: Text(text),
      activeColor: colorPrimary,
      checkColor: colorPrimary,
      value: isValue,
      onChanged: onChanged,
      // controlAffinity: ListTileControlAffinity.leading
    );
  }
}
