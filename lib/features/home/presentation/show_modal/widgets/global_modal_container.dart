import 'package:flutter/material.dart';

class GlobalModalContainer extends StatelessWidget {
  const GlobalModalContainer({Key? key, required this.child}) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Container(child: child);
  }
}
