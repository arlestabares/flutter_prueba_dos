import 'package:flutter/material.dart';

import '../../../../utils/const.dart';

class RowIconButtonClosed extends StatelessWidget {
  const RowIconButtonClosed({
    Key? key,
    required this.onPressed,
  }) : super(key: key);
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        IconButton(
          onPressed: onPressed,
          icon: Container(
            height: 25.0,
            width: 25.0,
            decoration: BoxDecoration(
              color: colorButtonClosed,
              borderRadius: BorderRadius.circular(21.0),
            ),
            child: const Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }
}
