import 'package:flutter/material.dart';

import '../../../../utils/const.dart';

class CheckBoxContainer extends StatelessWidget {
  const CheckBoxContainer({
    Key? key,
    this.isValue = false,
    required this.onChanged,
    required this.text,
    required this.isCheckbox,
  }) : super(key: key);

  final bool isValue;
  final Function(bool? value)? onChanged;
  final String text;
  final bool isCheckbox;
  @override
  Widget build(BuildContext context) {
    const style = TextStyle(fontSize: 16.0);
    return SizedBox(
      height: isCheckbox ? 50 : 100,
      child: LimitedBox(
        child: Row(
          children: <Widget>[
            Text(text, style: style),
            const Spacer(),
            Container(
              height: 25.0,
              width: 25.0,
              decoration: BoxDecoration(
                  border: Border.all(color: colorPrimary),
                  borderRadius: BorderRadius.circular(21.0)),
              child: Checkbox(
                activeColor: colorPrimary,
                checkColor: colorPrimary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                value: isValue,
                onChanged: onChanged,
              ),
            )
          ],
        ),
      ),
    );
  }
}
