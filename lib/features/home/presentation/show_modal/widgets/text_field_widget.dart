import 'package:flutter/material.dart';

import '../../../../utils/const.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget({
    Key? key,
    required this.onTap,
  }) : super(key: key);
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return TextField(
      onTap: onTap,
      decoration: InputDecoration(
        hintText: 'Direccion',
        border: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.black54,
            width: 2.0,
          ),
          borderRadius: BorderRadius.circular(21.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(21.0),
          borderSide: const BorderSide(color: colorButtonClosed, width: 2.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(21.0),
          borderSide: const BorderSide(
            color: Colors.black54,
            width: 2.0,
          ),
        ),
        suffixIcon: const Icon(
          Icons.search_rounded,
          color: colorButtonClosed,
        ),
      ),
    );
  }
}