import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart'
    as home_bloc;

import '../../../utils/const.dart';
import '../widgets/widgets.dart';
import 'widgets/widgets_modal.dart';

Future<void> showModalMarcas(BuildContext context, Size size) {
  bool isLakanto = false;
  bool isLakanto2 = false;
  bool isNutrishake = false;
  bool isNutrishake2 = false;
  bool isMarcaComercial = false;
  bool isSnaksanto = false;

  return showModalBottomSheet<void>(
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(21.0),
        topRight: Radius.circular(21.0),
      ),
    ),
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder:
            (BuildContext context, void Function(void Function()) setState) {
          return Container(
            padding: const EdgeInsets.symmetric(horizontal: 1.0),
            height:
                (size.width > 420.0) ? size.height * 0.58 : size.height * 0.76,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Container(
                          height: 25.0,
                          width: 25.0,
                          decoration: BoxDecoration(
                            color: colorButtonClosed,
                            borderRadius: BorderRadius.circular(21.0),
                          ),
                          child: const Icon(
                            Icons.close,
                            color: Colors.white,
                          )),
                    )
                  ],
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const <Widget>[
                          TextWidget(
                            text: 'Marcas',
                          ),
                        ],
                      ),
                      const SizedBox(height: 21.0),
                      GlobalModalContainer(
                        child: CheckBoxListTileContainer(
                          text: 'Lakanto',
                          isValue: isLakanto,
                          onChanged: (value) {
                            setState(() {
                              isLakanto = value!;
                            });
                            context
                                .read<home_bloc.Bloc>()
                                .add(home_bloc.LakantoProductEvent());
                          },
                        ),
                      ),
                      GlobalModalContainer(
                        child: CheckBoxListTileContainer(
                          text: 'Nutrishake',
                          isValue: isNutrishake,
                          onChanged: (value) {
                            setState(() {
                              isNutrishake = value!;
                            });
                            context
                                .read<home_bloc.Bloc>()
                                .add(home_bloc.NutriShakeProductEvent());
                          },
                        ),
                      ),
                      GlobalModalContainer(
                          child: CheckBoxListTileContainer(
                        text: 'Marca comercial',
                        isValue: isMarcaComercial,
                        onChanged: (value) {
                          setState(() {
                            isMarcaComercial = value!;
                          });
                          context
                              .read<home_bloc.Bloc>()
                              .add(home_bloc.MarcaComercialProductEvent());
                        },
                      )),
                      GlobalModalContainer(
                        child: CheckBoxListTileContainer(
                          text: 'Snacksanto',
                          isValue: isSnaksanto,
                          onChanged: (value) {
                            setState(() {
                              isSnaksanto = value!;
                            });
                            context
                                .read<home_bloc.Bloc>()
                                .add(home_bloc.SnackSantoProductEvent());
                          },
                        ),
                      ),
                      GlobalModalContainer(
                        child: CheckBoxListTileContainer(
                          text: 'Lakanto',
                          isValue: isLakanto2,
                          onChanged: (value) {
                            setState(() {
                              isLakanto2 = value!;
                            });
                            context
                                .read<home_bloc.Bloc>()
                                .add(home_bloc.LakantoProductEvent());
                          },
                        ),
                      ),
                      GlobalModalContainer(
                        child: CheckBoxListTileContainer(
                          text: 'Nutrishake',
                          isValue: isNutrishake2,
                          onChanged: (value) {
                            setState(() {
                              isNutrishake2 = value!;
                            });
                            context
                                .read<home_bloc.Bloc>()
                                .add(home_bloc.NutriShakeProductEvent());
                          },
                        ),
                      ),
                      const SizedBox(height: 21.0),
                      ElevatedButtonWidget(
                        text: "Filtrar(  productos)",
                        onPressed: () {},
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        },
      );
    },
  );
}
