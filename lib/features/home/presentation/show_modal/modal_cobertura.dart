import 'package:flutter/material.dart';
import 'package:prueba_dos/features/home/presentation/googlemap/google_map_page.dart';
import 'package:prueba_dos/features/home/presentation/show_modal/widgets/row_icon_button_close.dart';
import 'package:prueba_dos/features/home/presentation/show_modal/widgets/widgets_modal.dart';


Future<void> showModalCobertura(BuildContext context, Size size) {
  return showModalBottomSheet<void>(
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(21.0),
        topRight: Radius.circular(21.0),
      ),
    ),
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder:
            (BuildContext context, void Function(void Function()) setState) {
          return SizedBox(
            height:
                (size.width > 420.0) ? size.height * 0.7 : size.height * 0.8,
            child: Column(
              children: <Widget>[
                RowIconButtonClosed(onPressed: () => Navigator.pop(context)),
                const TitleAndSubtitleContainer(),
                const SizedBox(height: 12.0),
                SizedBox(
                  height: (size.width > 420.0)? size.height * 0.3: size.height *  0.3,
                  width: double.infinity,
                  child: GoogleMapPage(),
                ),
                const SizedBox(height: 16.0),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    children: [
                      TextFieldWidget(
                        onTap: () {},
                      ),
                      const SizedBox(height: 16.0),
                      Row(
                        children: [
                          Image.asset('assets/images/ubicacion.png'),
                          const SizedBox(width: 8.0),
                          TextButtonWidget(
                            onPressed: () {},
                          ),
                        ],
                      ),
                      const SizedBox(height: 21.0),
                      ElevatedButtonWidget(
                        text: "Continuar",
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      );
    },
  );
}

