import 'package:flutter/material.dart';
import 'package:prueba_dos/features/home/presentation/views/home_view.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const HomeView();
  }
}
