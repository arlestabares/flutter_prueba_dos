import 'package:flutter/material.dart';

import '../../../utils/const.dart';

class EtiquetaCard extends StatelessWidget {
  const EtiquetaCard({Key? key, required this.etiqueta}) : super(key: key);
  final String etiqueta;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.only(left: kDefaultPadding / 2),
      padding: const EdgeInsets.symmetric(
        horizontal: kDefaultPadding / 2.5,
        vertical: kDefaultPadding / 4,
      ),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black26),
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text(
        etiqueta,
        style: TextStyle(color: kTextColor.withOpacity(0.8), fontSize: 16),
      ),
    );
  }
}
