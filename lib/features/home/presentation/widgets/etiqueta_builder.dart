import 'package:flutter/material.dart';
import 'package:prueba_dos/features/home/presentation/widgets/widgets.dart';

class EtiquetasBuilder extends StatelessWidget {
  const EtiquetasBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> etiquetas = [
      "Etiqueta a",
      "Etiqueta bc",
      "Snacks",
      "Etiqueta b ",
    ];
    return Container(
      height: 36.0,
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: ListView.builder(
        // shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: etiquetas.length,
        itemBuilder: (context, index) {
          return EtiquetaCard(etiqueta: etiquetas[index]);
        },
      ),
    );
  }
}
