import 'package:flutter/material.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  const BottomNavigationBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      elevation: 0.5,
      items: [
        BottomNavigationBarItem(
          icon: Image.asset(
            'assets/images/leaf_fill.png',
            height: 31,
            width: 31,
          ),
          // icon: Icon(Icons.pin_drop, size: 41,),
          label: 'Comidas',
        ),
        BottomNavigationBarItem(
          icon: Image.asset('assets/images/vector.png'),
          label: 'Market',
        ),
      ],
    );
  }
}
