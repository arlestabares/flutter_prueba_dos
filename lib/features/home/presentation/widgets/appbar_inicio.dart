import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_dos/features/home/presentation/bloc/bloc.dart'
    as home_bloc;
import 'package:prueba_dos/features/utils/const.dart';

AppBar appBarInicio() {
  return AppBar(
    backgroundColor: Colors.grey.withOpacity(0.1),
    leading: const Icon(Icons.home),
    title: const Text('Inicio'),
    centerTitle: false,
    actions: [
      BlocBuilder<home_bloc.Bloc, home_bloc.State>(
        builder: (context, state) {
          return Container(
            margin: const EdgeInsets.only(right: 12.0),
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            width: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(11.0),
              border: Border.all(color: colorPrimary),
            ),
            child: Row(
              children: [
                Image.asset('assets/images/shape.png'),
                const SizedBox(width: 18.0),
                const Text(
                  '0',
                  style: TextStyle(fontSize: 21.0, color: colorPrimary),
                ),
              ],
            ),
          );
        },
      )
    ],
  );
}
