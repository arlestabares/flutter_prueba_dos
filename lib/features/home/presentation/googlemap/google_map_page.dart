import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapPage extends StatelessWidget {
  late GoogleMapController mapController;
  final LatLng _center = const LatLng(45.521563, -122.677433);
  //
  GoogleMapPage({Key? key}) : super(key: key);
  final Completer<GoogleMapController> _controller = Completer();
  final CameraPosition _cameraPosition = const CameraPosition(
    target: LatLng(4.5625952251, -75.75040712794193),
    zoom: 14.5,
  );
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  // final CameraPosition _position = const CameraPosition(target: )
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          GoogleMap(
            mapType: MapType.hybrid,
            initialCameraPosition: _cameraPosition,
            onMapCreated: (GoogleMapController controller) =>
                _controller.complete(controller),
          ),
        ],
      ),
    );
  }
}
