import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:prueba_dos/features/blocs/multi_bloc_widget.dart';
import 'package:prueba_dos/features/repositories/repository_provider.dart';
import 'package:prueba_dos/routes/routes.dart';

void main() {
  if (defaultTargetPlatform == TargetPlatform.android) {
    AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProviderWidget(
      child: MultiBlocProviderWidget(
        child: MaterialApp(
          title: 'Material App',
          theme: ThemeData(useMaterial3: true),
          initialRoute: 'home_page',
          routes: appRoutes,
        ),
      ),
    );
  }
}
