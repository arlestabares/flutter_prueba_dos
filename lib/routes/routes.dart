import 'package:flutter/material.dart';

import '../features/home/presentation/googlemap/google_map_page.dart';
import '../features/home/presentation/pages/home_page.dart';
import '../features/home/presentation/views/home_view.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home_page': (_) => const HomePage(),
  'home_view': (_) => const HomeView(),
  'google_map_page': (_) => GoogleMapPage(),
};
